---
title: Readme
created: '2020-05-23T08:02:55.660Z'
modified: '2020-06-29T04:12:36.925Z'
---

## Description

UI designs designed using Gravit Designer which can be installed in Windows, Linux and macOS. The examples here shows a visualisation of potential website designs (in which the final website build may differ). Examples marked with (\*) indicates that there is an example code built, written in HTML and CSS.

**Note**: All of the icons used are from FontAwesome

## Images

# Example 1

| Sample pictures                        | Sample pictures                        |
| -------------------------------------- | -------------------------------------- |
| ![Alt Text](./Example%201/image_1.png) | ![Alt Text](./Example%201/image_2.png) |

# Example 2 (*)

Live demo available [here](https://login.pongsakornsurasarang.xyz/)

![Alt Text](./Example%202/Login%20Design.png)


# Example 3

![Alt Text](./Example%203/Chat%20UI.png)

# Example 4

![Alt Text](./Example%204/Todo%20List.png)

# Example 5 (*)

Live demo available [here](https://lyrics.pongsakornsurasarang.xyz/)

| Sample pictures                       | Sample pictures                            |
| ------------------------------------- | ------------------------------------------ |
| ![Alt Text](./Example%205/Lyrics.png) | ![Alt Text](./Example%205/Lyrics_dark.png) |

# Example 6

![Alt Text](./Example%206/Portfolio.png)

# Example 7

Live demo available [here](https://trello.pongsakornsurasarang.xyz/)

![Alt Text](./Example%207/Vue-Trello.png)
